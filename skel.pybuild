# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

# NOTE: The comments in this file are for instruction and documentation.
# They should not appear in the final pybuild.
# For more information on available functions and variables and their uses,
# see the Portmod wiki

# imports the basic Pybuild1 class. All mods should implement this class.
from portmod.pybuild import Pybuild1, InstallDir, File

# Special import that creates values based on the current file
# M refers to the mod's name and version. E.g. foo-example-1.0
from portmod.pybuild.modinfo import M


class Mod(Pybuild1):
    # Full name of the mod
    NAME = "Skeleton"

    # Short one-line description of this mod
    DESC = "This is a sample skeleton pybuild file"

    # Homepage of the mod. Not used directly, but helpful for reference
    # and appears in searches
    HOMEPAGE = "https://foo.example.org/"

    # Links to any requires sources which will be automatically downloaded
    SRC_URI = f"https://foo.example.org/{M}.zip"

    # License of the mod. This must match the name of a file in the
    # licenses directory. If the license does not already exist in the directory,
    # please add it
    LICENSE = ""

    # Keywords indicate the stability of the mod on the given platforms.
    # A tilde should prefix the platform if the mod is considered unstable.
    # Omitting a keyword indicates that it is untested
    KEYWORDS = "openmw ~tes3mp"

    # Runtime dependencies. I.e. any mods that are required by this mod to
    # run properly.
    RDEPEND = ""

    # List of all use flags in the pybuild. Not needed if the pybuild does not use
    # use flags. Special flags such as texture_size_NUM do not need to be included.
    IUSE = ""

    # List of directories that portmod will install. The first parameter is the location
    # relative to the root of the source file, and SOURCE can be omitted if there is
    # only one source defined in the pybuild.
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("Example.omwaddon")], SOURCE=f"{M}.zip")
    ]
