# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from typing import Optional

import os
from portmod import use_reduce


class Git:
    """
    Pybuild Class that directly fetches from Git repos

    Subclasses should specify GIT_SRC_URI, containing a use-reduce-able
    list of remote git repositories
    Optionally, GIT_BRANCH, GIT_COMMIT and GIT_COMMIT_DATE can be used to specify
    what branch and commit should be used.
    """

    GIT_SRC_URI: str
    GIT_BRANCH: Optional[str] = None
    GIT_COMMIT: Optional[str] = None
    GIT_COMMIT_DATE: Optional[str] = None

    def src_unpack(self):
        repos = use_reduce(self.GIT_SRC_URI, self.USE, [], is_src_uri=True, flat=True)
        self.INSTALLED_COMMIT = {}

        for repo in repos:
            name, _ = os.path.splitext(os.path.basename(repo))
            if self.GIT_COMMIT:
                self.execute(f"git clone {repo}")
                os.chdir(name)
                self.execute(f"git checkout {self.GIT_COMMIT}")
            elif self.GIT_COMMIT_DATE:
                self.execute(f"git clone {repo}")
                os.chdir(name)
                branch = self.GIT_BRANCH or self.execute(
                    "git rev-parse --abbrev-ref HEAD", pipe_output=True
                )
                checkout_str = branch + "@" + "{" + self.GIT_COMMIT_DATE + "}"
                self.execute(f"git checkout {checkout_str}")
            elif self.GIT_BRANCH:
                self.execute(
                    f"git clone {repo} --depth 1 "
                    f"--branch {self.GIT_BRANCH} --shallow-submodules"
                )
            else:
                self.execute(f"git clone {repo} --depth 1 --shallow-submodules")

            self.INSTALLED_COMMIT[repo] = self.execute(
                "git rev-parse HEAD", pipe_output=True
            )

    def can_update_live(self):
        repos = use_reduce(self.GIT_SRC_URI, self.USE, [], is_src_uri=True, flat=True)

        for repo in repos:
            newhash = self.execute(f"git ls-remote {repo}").split()[0]
            oldhash = self.get_installed_env()["INSTALLED_COMMIT"][repo]
            if oldhash != newhash:
                return True

        return False
