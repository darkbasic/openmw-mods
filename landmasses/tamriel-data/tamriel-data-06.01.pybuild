# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from portmod.pybuild import Pybuild1, InstallDir, File, DOWNLOAD_DIR
from portmod.pybuild.modinfo import MV

NMV = MV.replace(".", "-")


class Mod(Pybuild1):
    NAME = "Tamriel Data"
    DESC = "Adds game data files required by several landmass addition mods"
    HOMEPAGE = "http://www.tamriel-rebuilt.org"
    LICENSE = "tamriel-data-06.01"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        texture_size_512? ( Tamriel_Data_(vanilla)_v{MV}.7z )
        legacy? ( Tamriel_Data_Legacy-44537-{NMV}.7z )
        texture_size_1024? ( Tamriel_Data_(HD)_v{MV}.7z )
    """
    IUSE = "legacy"
    RESTRICT = "fetch"
    TIER = 2

    TEXTURE_SIZES = "512 1024"
    INSTALL_DIRS = [
        # InstallDir(".", SOURCE="Tamriel_Data_Animkit_Meshes-44537-1-0a.7z"),
        InstallDir(
            "Tamriel_Data (vanilla) v06/Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("PT_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            SOURCE=f"Tamriel_Data_(vanilla)_v{MV}.7z",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            "Data Files",
            ARCHIVES=[File("TR_Data.bsa"), File("pt_data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            SOURCE=f"Tamriel_Data_(HD)_v{MV}.7z",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                # Note that Tamriel_Data_Legacy should not be enabled in game.
                # The legacy option is included as a development tool.
                # File("Tamriel_Data_Legacy.esm")
            ],
            SOURCE=f"Tamriel_Data_Legacy-44537-{NMV}.7z",
            REQUIRED_USE="legacy",
        ),
    ]

    def mod_nofetch(self):
        print("Please download the following files from the urls at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {DOWNLOAD_DIR}")
        print()
        for source in self.A:
            print(f"  {source}")
        print()
        if "legacy" in self.USE:
            print("  https://www.nexusmods.com/morrowind/mods/44537/?tab=files")
        if "texture_size_512" in self.USE:
            print(
                "  https://drive.google.com/open?id=1ES_p8wGpGXdr2YwkwRTAtRxcPGNI_oI4"
            )
        elif "texture_size_1024" in self.USE:
            print(
                "  https://drive.google.com/open?id=18nU0b_0mMUy_-vpK3gZfU0psLYtS_ImL"
            )
